package development.lechuga.certamen;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import development.lechuga.certamen.Fragmentos.DatosContactoFragment;
import development.lechuga.certamen.Fragmentos.DatosInicioSesionFragment;
import development.lechuga.certamen.Fragmentos.DatosPersonalesFragment;

public class MainActivity extends AppCompatActivity implements
        DatosInicioSesionFragment.OnFragmentInteractionListener,
        DatosContactoFragment.OnFragmentInteractionListener,
        DatosPersonalesFragment.OnFragmentInteractionListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        DatosPersonalesFragment datosPersonalesFragment = new DatosPersonalesFragment();

        transaction.replace(R.id.flcontenedor, datosPersonalesFragment) .commit();
    }

    @Override
    public void onFragmentInteraction(String NombreFragmento, String evento) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        //Pasar a datos de contacto
        if(NombreFragmento.equals("DatosPersonalesFragment")){
            if(evento.equals("BTN_SIGUIENTE_CLICK")){
                DatosContactoFragment fragment = new DatosContactoFragment();
                transaction.replace(R.id.flcontenedor,fragment).commit();
            }
        }

        // Pasar a datos de inicio de sesion
        if(NombreFragmento.equals("DatosContactoFragment")){
            if(evento.equals("BTN_SIGUIENTE_CLICK")){
                // TODO: Pasar al siguiente fragmento
                DatosInicioSesionFragment fragment = new DatosInicioSesionFragment();
                transaction.replace(R.id.flcontenedor,fragment).commit();
            }
        }

        // Mostrar contrasena
        if(NombreFragmento.equals("DatosInicioSesionFragment")){
            if(evento.equals("BTN_SIGUIENTE_CLICK")){
                // TODO: Mostrar contrasena
                // La verificacion se hace desde el fragmento

            }
        }

    }


}
